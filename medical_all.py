# -*- coding: utf-8 -*-


"""
Created on Mon Jan 16 11:41:46 2017

@author: robin
"""

import inspect
import os
from nifty import *
import numpy as np

from nifty.basic_arithmetics import _math_helper
import plotly.offline as pl
import plotly.graph_objs as go

from sugar import *
from nifty_los_response import LOSResponse
from los_creator import LinesOfSight
from corr_field_drawer import CorrFieldsDrawer
from component_prior import Component_Prior
from component_posterior import Component_Posterior

# Create mock signal and covariance
rand_seed = 106
np.random.seed(rand_seed)

""" Set nifty stuff """
distribution_strategy = "not"
n_components = 2

total_volume = 1.
npix = 256
dimensions = [npix, npix]
s_space = RGSpace(dimensions, distances=total_volume / npix, zerocenter=False)
operator_space = RGSpace(dimensions+[n_components], zerocenter=False)

fft = FFTOperator(s_space, target_dtype=np.complex128)
h_space = RGRGTransformation.get_codomain(s_space)
p_space = PowerSpace(h_space, distribution_strategy=distribution_strategy)

k_vec_length = p_space.shape[0]

<<<<<<< HEAD
#sigm_short
corr_length_k_short = int(k_vec_length * 0.0002) #0.0002 = short, 0.004=long
corr_length_k_long = int(k_vec_length * 0.004)

corr_lengths = {'sigm_short' :      corr_length_k_short,
                'strt_short' :      corr_length_k_short,
                'sigm_long' :       corr_length_k_long,
                'strt_long_' :       corr_length_k_long}


#correlations = ['sigm_long_priorOFF', 'strt_short_priorOFF', 'strt_long_priorOFF',
#               'sigm_short_priorOFF', 'strt_short', 'strt_long_', 'sigm_short', 'sigm_long']
correlations = ['sigm_short_priorOFF']
for correlation in correlations:

    if correlation[:4] == 'strt':
        print "corr is ", correlation, ". Entered 'straight' if-statement. Correct?"

        """ Define the cross correlation """
        def corr(k, corr_length_k):
=======
corr_length_k = int(k_vec_length * 0.0002)

def corr(k, corr_length_k):
    sig_height = 0.75
    sig_translation = corr_length_k
    sig_k = 2 * ((1. / (1 + np.exp(k - k[sig_translation]))) - 0.4)
    sig_k = sig_k * sig_height
    return sig_k

def cross_pow_spectrum(k, pow1, pow2):
    result = np.sqrt(pow1(k) * pow2(k))
    result = result * corr(k, corr_length_k)
    return result


correlations = ['sig_corr', 'straight_corr']
for correlation in correlations:


    pow_spectra = {}
    pow_spectra['pow1'] = (lambda k: .0003 / ((k / 4. + .5) ** 5))
    pow_spectra['pow2'] = (lambda k: .0002 / ((k / 4. + .5) ** 5))
    pow_spectra['pow12real'] = (lambda k: cross_pow_spectrum(k, pow_spectra['pow1'],
                                                             pow_spectra['pow2']))
    pow_spectra['pow12imag'] = (lambda k: k * 0.)

    corr_jump = 0
    if correlation == 'straight_corr':
        corr_length_k = int(k_vec_length*0.) #0.002

        """ Define the cross correlation """
        def corr(k,corr_length_k):
>>>>>>> fa22a386b7ff37edf57d27de6cd08539775db5f7
            a = np.zeros_like(k)
            a = a + 0.80
            a[corr_length_k:] = -a[corr_length_k:]
            return a

<<<<<<< HEAD
    if correlation[:4] == 'sigm':
        print "corr is ", correlation, ". Entered 'sigm' if-statement. Correct?"

        """ Define the cross correlation """
        def corr(k, corr_length_k):
            sig_height = 0.8
            sig_translation = corr_length_k
            sig_k = 2 * ((1. / (1 + np.exp(k - k[sig_translation]))) - 0.5)
            sig_k = sig_k * sig_height
            return sig_k

    def cross_pow_spectrum(k, pow1, pow2 ):
        result = np.sqrt(pow1(k)*pow2(k))
        result = result*corr(k, corr_lengths[correlation[:len('strt_long_')]])
        return result

    pow_spectra = {}
    pow_spectra['pow1'] = (lambda k: .0009/((k/4.+.5)**5))
    pow_spectra['pow2'] = (lambda k: .0006/((k/4.+.5)**5))
    pow_spectra['pow12real'] = (lambda k: cross_pow_spectrum(k, pow_spectra['pow1'],
                                 pow_spectra['pow2'] ))
    pow_spectra['pow12imag'] = (lambda k: k*0.)
=======
        def cross_pow_spectrum(k, pow1, pow2 ):
            result = np.sqrt(pow1(k)*pow2(k))
            result = result*corr(k, corr_length_k)
            return result

        pow_spectra = {}
        pow_spectra['pow1'] = (lambda k: .0009/((k/4.+.5)**5))
        pow_spectra['pow2'] = (lambda k: .0006/((k/4.+.5)**5))
        pow_spectra['pow12real'] = (lambda k: cross_pow_spectrum(k, pow_spectra['pow1'],
                                     pow_spectra['pow2'] ))
        pow_spectra['pow12imag'] = (lambda k: k*0.)
>>>>>>> fa22a386b7ff37edf57d27de6cd08539775db5f7


    """ Draw correlated fields """
    fieldDrawer = CorrFieldsDrawer(pow_space=p_space, distr_strat=distribution_strategy, n_components=n_components)
    sh1, sh2 = fieldDrawer.draw_fields(pow_spectra, rand_seed=rand_seed)
    harmonic_signals = [sh1, sh2]
    signal_list = [fft.inverse_times(sh1), fft.inverse_times(sh2)]

    signals_vals = np.empty(dimensions+[n_components])
    for component in xrange(n_components):
        signals_vals[...,component] = signal_list[component].val

    signals = Field(operator_space, val=signals_vals, distribution_strategy=distribution_strategy)
    exp_signals = exp(signals)

    losCreator = LinesOfSight()


<<<<<<< HEAD
    b_is = [4000000, 3000000] #10 million, 1 million
    los = [250]
    measurements_list = [2]
=======
    b_is = [10000000]
    los = [200]
    measurements = 2
>>>>>>> fa22a386b7ff37edf57d27de6cd08539775db5f7

    mu_E1 = [1.5, 0.25]
    mu_E2 = [.5, 0.4]
    mu_E3 = [0.3, 0.55]
    mu_E4 = [0.4, 0.3]
    mu_E5 = [1.2, 0.2]
    mu_E6 = [.3, .4]
    mu_E7 = [.2, .21]
    mu_Es = [mu_E1, mu_E2, mu_E3, mu_E4, mu_E5, mu_E6, mu_E7]

    for b_i in b_is:
        for lines in los:
<<<<<<< HEAD
            for measurements in measurements_list:

                linesPerAngle = lines
                anglesPerSetup = 100


                foldername_plots = 'bi{1}mil_{7}_muE1-{2}-{3}_muE2-{4}-{5}_los{0}_{6}measures'.format(anglesPerSetup*linesPerAngle,
                                                                        b_i/1000000, mu_E1[0], mu_E1[1], mu_E2[0], mu_E2[1], measurements,
                                                                        correlation)

                if not os.path.exists(foldername_plots):
                    os.makedirs(foldername_plots)

                """ Turn off/on prior knowledge of covariance """
                if correlation[-3:] == 'OFF':
                    pow_spectra['pow12real'] = (lambda k: k * 0.)
                else:
                    pow_spectra['pow12real'] = (lambda k: cross_pow_spectrum(k, pow_spectra['pow1'],
                                                                             pow_spectra['pow2']))

                """ initialize prior """
                S = Component_Prior(pow_spectra, operator_space, h_space, s_space, fft, distribution_strategy)

                """ initialize response """
                data_vec_length = anglesPerSetup*linesPerAngle
                starts, ends = losCreator.create_RotatedScanSetup(anglesPerSetup, linesPerAngle)
                R = LOSResponse(s_space, starts=starts, ends=ends,sigmas_up=0.01, sigmas_low=0.01,
                                distribution_strategy=distribution_strategy)

                """ Plotting routine for the signals """
                s_data_combined = 0
                exp_data_combined_sig = {}
                for e_channel in range(measurements):
                    exp_data_combined_sig['mu_E{}'.format(e_channel)] = 0
                for component in range(n_components):
                    print 'plotting for s{}'.format(component)
                    s_data = signals.val[:, :, component].get_full_data().real
                    exp_ss_data = exp_signals.val[:, :, component].get_full_data().real
                    pl.plot([go.Heatmap(z=s_data)], filename= (foldername_plots +'/signal{0}.html'.format(component)),
                            auto_open=False)
                    pl.plot([go.Heatmap(z=exp_ss_data)], filename=(foldername_plots +'/expsignal{0}.html'.format(component)),
                            auto_open=False)
                    s_data_combined = s_data_combined + s_data

                    for e_channel in range(measurements):
                        mu_E = mu_Es[e_channel]
                        exp_data_combined_sig['mu_E{}'.format(e_channel)] += exp_ss_data*mu_E[component]


                    if component == 1:
                        pl.plot([go.Heatmap(z=s_data_combined)], filename=(foldername_plots +'/signals_combined.html'),
                                auto_open=False)
                        for e_channel in range(measurements):
                            pl.plot([go.Heatmap(z=exp_data_combined_sig['mu_E{}'.format(e_channel)])],
                                    filename=(foldername_plots + '/expsignals_combined_muE{}.html'.format(e_channel)),
                                    auto_open=False)

                #background = 1
                #background = [np.random.poisson(background) for i in xrange(data_vec_length)]
                background = [0 for i in range(data_vec_length)]

                """ calculate absorption factor """
                absorption_factor = [0 for i in range(measurements)]
                absorption = [0 for i in range(measurements)]
                data = [0 for i in range(measurements)]

                for e_channel in range(measurements):
                    mu_E = mu_Es[e_channel]
                    for component in range(n_components):
                        exp_signal = Field(domain=s_space, val=exp_signals.val[:,:,component],
                                       distribution_strategy=distribution_strategy, copy=False)
                        absorption_factor[e_channel] += mu_E[component]*exp_signal

                    absorption[e_channel] = exp(-R(absorption_factor[e_channel]))
                    poisson_rate = b_i * absorption[e_channel]
                    data[e_channel] = drawPoissonDistr(poisson_rate)

                    """ plot data histogram """
                    plot_data = [go.Histogram(x=absorption[e_channel].val.get_full_data().real)]
                    pl.plot(plot_data, filename= (foldername_plots +
                                             '/absorption_histogram_mu1-{}_mu2-{}_channel{}.html'.format(
                                             mu_E1[0], mu_E1[1], e_channel)), auto_open= False)
                    plot_data = [go.Histogram(x=data[e_channel].val.get_full_data().real)]
                    pl.plot(plot_data, filename=(foldername_plots +
                                                 '/data_histogram_mu1-{}_mu2-{}_channel{}.html'.format(
                                                     mu_E1[0], mu_E1[1], e_channel)), auto_open=False)

                ''' initialize map for first guess  '''
                fieldDrawer = CorrFieldsDrawer(pow_space=p_space, distr_strat=distribution_strategy, n_components=n_components)
                mh1, mh2 = fieldDrawer.draw_fields(pow_spectra, rand_seed=156)
                maps_list = [fft.inverse_times(mh1)  , fft.inverse_times(mh2)]
                maps_vals = np.empty(dimensions+[n_components])
                for component in xrange(n_components):
                    maps_vals[:,:,component] = maps_list[component].val*0

                maps = Field(operator_space, val=maps_vals, distribution_strategy=distribution_strategy)

                '''
                maps = Field.from_random(domain=operator_space, random_type='normal', std=1, mean=0)
                '''

                old_E=0
                learning_rate = .4
                train_steps = 21.
                for i in range(int(train_steps)):
                    print 'this is step ' + str(i)

                    exp_maps = exp(maps)
                    #returns a list conaining one lamba-field per energy channel/measurement
                    lambda_m = lambda_m_calc(b_i, R, mu_Es, exp_maps, measurements, s_space, distribution_strategy)

                    likeli_E, prior_E, total_E = calc_Energy(lambda_m, data, maps, S, n_components, measurements, s_space, distribution_strategy)
                    print 'likelihood E: ', likeli_E , 'prior E: ', prior_E, 'Total_E', total_E
                    print 'change in Tot E: ', total_E-old_E
                    old_E = total_E

                    mapForces = map_ForcesCalc(S, R, maps, exp_maps, data, lambda_m, mu_Es, measurements, s_space, distribution_strategy)
                    D = Component_Posterior(S, R, exp_maps, lambda_m, mu_Es, b_i, data, measurements, s_space,
                                 distribution_strategy)
                    map_steps = D(mapForces)
                    maps = maps + learning_rate*map_steps

                    #regular plotting during optimization

                    print('MapForces: ', mapForces.val.get_full_data().min(), mapForces.val.get_full_data().max())
                    print('MapSteps', map_steps.val.get_full_data().min(), map_steps.val.get_full_data().max())

                    if i % 5==0 or i in [1,2,3]:
                        m_data_combined = 0
                        exp_m_data_combined = {}
                        for e_channel in xrange(measurements):
                            exp_m_data_combined['mu_E{}'.format(e_channel)] = 0


                        for component in xrange(n_components):
                            print 'plotting for sig{}'.format(component)
                            m_data = maps.val[:,:,component].get_full_data().real
                            exp_m_data = exp_maps.val[:,:,component].get_full_data().real
                            pl.plot([go.Heatmap(z=m_data)], filename=(foldername_plots +
                                    '/map{0}_step{1}.html'.format(component, i)), auto_open=False)
                            pl.plot([go.Heatmap(z=exp_m_data)], filename=(foldername_plots +
                                    '/expmap{0}_step{1}.html'.format(component, i)), auto_open=False)
                            m_data_combined = m_data_combined + m_data

                            for e_channel in xrange(measurements):
                                mu_E = mu_Es[e_channel]
                                exp_m_data_combined['mu_E{}'.format(e_channel)] += exp_m_data * mu_E[component]

                            if component == 1:
                                pl.plot([go.Heatmap(z=m_data_combined)], filename=(foldername_plots +
                                        '/maps_combined_step{0}.html'.format(i)), auto_open=False)
                                for e_channel in xrange(measurements):
                                    pl.plot([go.Heatmap(z=exp_m_data_combined['mu_E{}'.format(e_channel)])],
                                            filename=(foldername_plots +'/expmaps_combined_step{0}_E{1}.html'.format(i, e_channel)
                                                      ), auto_open=False)

                            map = signal_list[0]*0
                            map.val = maps.val[:,:,component]
                            map_harmonic = fft(map)
                            trace0 = go.Scatter(
                                x=p_space.kindex,
                                y=map_harmonic.power_analyze().val.get_full_data().real,
                                mode='lines',
                                name='map{}_pow_spec'.format(component)
                            )
                            trace1 = go.Scatter(
                                x=p_space.kindex,
                                y=harmonic_signals[component].power_analyze().val.get_full_data().real,
                                mode='lines',
                                name='pow_spec of signal0')
                            trace2 = go.Scatter(
                                x=p_space.kindex,
                                y=pow_spectra['pow{}'.format(component+1)](p_space.kindex),
                                mode='lines',
                                name='pow_spec of signal1')

                            traces = [trace0, trace1, trace2]
                            pl.plot(traces, filename=foldername_plots +'/pow_spectra_comp{}_step{}.html'.format(component,i), auto_open=False)

                """ Another question to ask in the end: Does the reconstructed map produce the same data as the original signal? """
                """ both for the noisy and noiseless case """
                noiseless_data = lambda_m_calc(b_i, R, mu_Es, exp_signals, measurements, s_space, distribution_strategy)

                for e_channel in range(measurements):
                    print e_channel
                    print noiseless_data[e_channel]
                    print lambda_m[e_channel]
                    plot_data = [go.Histogram(x=((data[e_channel]-lambda_m[e_channel]).val.get_full_data().real)/data[
                        e_channel].val.get_full_data().real)]
                    pl.plot(plot_data, filename= (foldername_plots +'/final_comparison_NoisyData_channel{}.html'.format(
                                                 e_channel)), auto_open= False)
                    plot_data = [go.Histogram(x=((noiseless_data[e_channel] - lambda_m[e_channel]).val.get_full_data().real
                                                 ) / noiseless_data[e_channel].val.get_full_data().real)]
                    pl.plot(plot_data, filename=(foldername_plots + '/final_comparison_IdealData_channel{}.html'.format(
                        e_channel)), auto_open=False)
=======
            linesPerAngle = lines
            anglesPerSetup = 200


            foldername_plots = '{7}_high-res_muE1-{2}-{3}_muE2-{4}-{5}_los{0}_bi{1}_{6}channels'.format(anglesPerSetup*linesPerAngle,
                                                                    b_i, mu_E1[0], mu_E1[1], mu_E2[0], mu_E2[1], measurements,
                                                                    correlation)

            if not os.path.exists(foldername_plots):
                os.makedirs(foldername_plots)

            # comment below eradicates cross correlation for prior
            #pow_spectra['pow12real'] = (lambda k: k * 0.)
            """ initialize prior """
            S = Component_Prior(pow_spectra, operator_space, h_space, s_space, fft, distribution_strategy)

            """ initialize response """
            data_vec_length = anglesPerSetup*linesPerAngle
            starts, ends = losCreator.create_RotatedScanSetup(anglesPerSetup, linesPerAngle)
            R = LOSResponse(s_space, starts=starts, ends=ends,sigmas_up=0.01, sigmas_low=0.01,
                            distribution_strategy=distribution_strategy)

            """ Plotting routine for the signals """
            s_data_combined = 0
            exp_data_combined_sig = {}
            for e_channel in range(measurements):
                exp_data_combined_sig['mu_E{}'.format(e_channel)] = 0
            for component in range(n_components):
                print 'plotting for s{}'.format(component)
                s_data = signals.val[:, :, component].get_full_data().real
                exp_ss_data = exp_signals.val[:, :, component].get_full_data().real
                pl.plot([go.Heatmap(z=s_data)], filename= (foldername_plots +'/signal{0}.html'.format(component)),
                        auto_open=False)
                pl.plot([go.Heatmap(z=exp_ss_data)], filename=(foldername_plots +'/expsignal{0}.html'.format(component)),
                        auto_open=False)
                s_data_combined = s_data_combined + s_data

                for e_channel in range(measurements):
                    mu_E = mu_Es[e_channel]
                    exp_data_combined_sig['mu_E{}'.format(e_channel)] += exp_ss_data*mu_E[component]


                if component == 1:
                    pl.plot([go.Heatmap(z=s_data_combined)], filename=(foldername_plots +'/signals_combined.html'),
                            auto_open=False)
                    for e_channel in range(measurements):
                        pl.plot([go.Heatmap(z=exp_data_combined_sig['mu_E{}'.format(e_channel)])],
                                filename=(foldername_plots + '/expsignals_combined_muE{}.html'.format(e_channel)),
                                auto_open=False)

            #background = 1
            #background = [np.random.poisson(background) for i in xrange(data_vec_length)]
            background = [0 for i in range(data_vec_length)]

            """ calculate absorption factor """
            absorption_factor = [0 for i in range(measurements)]
            absorption = [0 for i in range(measurements)]
            data = [0 for i in range(measurements)]

            for e_channel in range(measurements):
                mu_E = mu_Es[e_channel]
                for component in range(n_components):
                    exp_signal = Field(domain=s_space, val=exp_signals.val[:,:,component],
                                   distribution_strategy=distribution_strategy, copy=False)
                    absorption_factor[e_channel] += mu_E[component]*exp_signal

                absorption[e_channel] = exp(-R(absorption_factor[e_channel]))
                poisson_rate = b_i * absorption[e_channel]
                data[e_channel] = drawPoissonDistr(poisson_rate)

                """ plot data histogram """
                plot_data = [go.Histogram(x=absorption[e_channel].val.get_full_data().real)]
                pl.plot(plot_data, filename= (foldername_plots +
                                         '/absorption_histogram_mu1-{}_mu2-{}_channel{}.html'.format(
                                         mu_E1[0], mu_E1[1], e_channel)), auto_open= False)
                plot_data = [go.Histogram(x=data[e_channel].val.get_full_data().real)]
                pl.plot(plot_data, filename=(foldername_plots +
                                             '/data_histogram_mu1-{}_mu2-{}_channel{}.html'.format(
                                                 mu_E1[0], mu_E1[1], e_channel)), auto_open=False)

            ''' initialize map for first guess  '''
            fieldDrawer = CorrFieldsDrawer(pow_space=p_space, distr_strat=distribution_strategy, n_components=n_components)
            mh1, mh2 = fieldDrawer.draw_fields(pow_spectra, rand_seed=156)
            maps_list = [fft.inverse_times(mh1)  , fft.inverse_times(mh2)]
            maps_vals = np.empty(dimensions+[n_components])
            for component in xrange(n_components):
                maps_vals[:,:,component] = maps_list[component].val*0

            maps = Field(operator_space, val=maps_vals, distribution_strategy=distribution_strategy)

            '''
            maps = Field.from_random(domain=operator_space, random_type='normal', std=1, mean=0)
            '''

            old_E=0
            learning_rate = .4
            train_steps = 21.
            for i in range(int(train_steps)):
                print 'this is step ' + str(i)

                exp_maps = exp(maps)
                #returns a list conaining one lamba-field per energy channel/measurement
                lambda_m = lambda_m_calc(b_i, R, mu_Es, exp_maps, measurements, s_space, distribution_strategy)

                likeli_E, prior_E, total_E = calc_Energy(lambda_m, data, maps, S, n_components, measurements, s_space, distribution_strategy)
                print 'likelihood E: ', likeli_E , 'prior E: ', prior_E, 'Total_E', total_E
                print 'change in Tot E: ', total_E-old_E
                old_E = total_E

                mapForces = map_ForcesCalc(S, R, maps, exp_maps, data, lambda_m, mu_Es, measurements, s_space, distribution_strategy)
                D = Component_Posterior(S, R, exp_maps, lambda_m, mu_Es, b_i, data, measurements, s_space,
                             distribution_strategy)
                map_steps = D(mapForces)
                maps = maps + learning_rate*map_steps

                #regular plotting during optimization

                print('MapForces: ', mapForces.val.get_full_data().min(), mapForces.val.get_full_data().max())
                print('MapSteps', map_steps.val.get_full_data().min(), map_steps.val.get_full_data().max())

                if i % 5==0 or i in [1,2,3]:
                    m_data_combined = 0
                    exp_m_data_combined = {}
                    for e_channel in xrange(measurements):
                        exp_m_data_combined['mu_E{}'.format(e_channel)] = 0


                    for component in xrange(n_components):
                        print 'plotting for sig{}'.format(component)
                        m_data = maps.val[:,:,component].get_full_data().real
                        exp_m_data = exp_maps.val[:,:,component].get_full_data().real
                        pl.plot([go.Heatmap(z=m_data)], filename=(foldername_plots +
                                '/map{0}_step{1}.html'.format(component, i)), auto_open=False)
                        pl.plot([go.Heatmap(z=exp_m_data)], filename=(foldername_plots +
                                '/expmap{0}_step{1}.html'.format(component, i)), auto_open=False)
                        m_data_combined = m_data_combined + m_data

                        for e_channel in xrange(measurements):
                            mu_E = mu_Es[e_channel]
                            exp_m_data_combined['mu_E{}'.format(e_channel)] += exp_m_data * mu_E[component]

                        if component == 1:
                            pl.plot([go.Heatmap(z=m_data_combined)], filename=(foldername_plots +
                                    '/maps_combined_step{0}.html'.format(i)), auto_open=False)
                            for e_channel in xrange(measurements):
                                pl.plot([go.Heatmap(z=exp_m_data_combined['mu_E{}'.format(e_channel)])],
                                        filename=(foldername_plots +'/expmaps_combined_step{0}_E{1}.html'.format(i, e_channel)
                                                  ), auto_open=False)

                        map = signal_list[0]*0
                        map.val = maps.val[:,:,component]
                        map_harmonic = fft(map)
                        trace0 = go.Scatter(
                            x=p_space.kindex,
                            y=map_harmonic.power_analyze().val.get_full_data().real,
                            mode='lines',
                            name='map{}_pow_spec'.format(component)
                        )
                        trace1 = go.Scatter(
                            x=p_space.kindex,
                            y=harmonic_signals[component].power_analyze().val.get_full_data().real,
                            mode='lines',
                            name='pow_spec of signal0')
                        trace2 = go.Scatter(
                            x=p_space.kindex,
                            y=pow_spectra['pow{}'.format(component+1)](p_space.kindex),
                            mode='lines',
                            name='pow_spec of signal1')

                        traces = [trace0, trace1, trace2]
                        pl.plot(traces, filename=foldername_plots +'/pow_spectra_comp{}_step{}.html'.format(component,i), auto_open=False)

            """ Another question to ask in the end: Does the reconstructed map produce the same data as the original signal? """
            """ both for the noisy and noiseless case """
            noiseless_data = lambda_m_calc(b_i, R, mu_Es, exp_signals, measurements, s_space, distribution_strategy)

            for e_channel in range(measurements):
                print e_channel
                print noiseless_data[e_channel]
                print lambda_m[e_channel]
                plot_data = [go.Histogram(x=((data[e_channel]-lambda_m[e_channel]).val.get_full_data().real)/data[
                    e_channel].val.get_full_data().real)]
                pl.plot(plot_data, filename= (foldername_plots +'/final_comparison_NoisyData_channel{}.html'.format(
                                             e_channel)), auto_open= False)
                plot_data = [go.Histogram(x=((noiseless_data[e_channel] - lambda_m[e_channel]).val.get_full_data().real
                                             ) / noiseless_data[e_channel].val.get_full_data().real)]
                pl.plot(plot_data, filename=(foldername_plots + '/final_comparison_IdealData_channel{}.html'.format(
                    e_channel)), auto_open=False)
>>>>>>> fa22a386b7ff37edf57d27de6cd08539775db5f7

print 'all done'