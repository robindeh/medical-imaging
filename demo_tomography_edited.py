# -*- coding: utf-8 -*-

from nifty import *
from nifty_los_response import LOSResponse
import numpy as np

import plotly.offline as pl
import plotly.graph_objs as go

from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.rank

from d2o import distributed_data_object,\
                STRATEGIES
from line_integrator import multi_integrator, \
                            gaussian_error_function
from nifty.config import nifty_configuration as nc,\
                         dependency_injector as gdi
import plotly.plotly as py
from nifty.field import Field
from nifty.field_types import FieldArray
from nifty.spaces import RGSpace
from imaging_response import ImagingResponse

from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.rank

from nifty.minimization import ConjugateGradient

if __name__ == "__main__":

    # Creates LOS that try to cover the whole rectangular RGSpace. The extra
    # -0.4 and 1.4 are chosen so that rotation of these lines still lie
    # outside the signal space.
    # Careful! line_starts[y,x] for some reason, rather than [x,y]. Therefore,
    # lines are spread along the x-axis, created with length 1.5 (in order to
    # cover the whole width of the signal space, as it is 1*1 and lines are
    # rotated later.)
    def create_LinesOfSight(number):
        line_starts = []
        line_ends = []
        for i in xrange(number):
            # lines should fall inbetween the i's, therefore add 0.5
            i = float(i+0.5)
            line_starts += [[(-0.25), (-0.25 + 1.5*i/number)]]
            line_ends +=   [[(1.25) , (-0.25 + 1.5*i/number)]]
        return np.array(line_starts), np.array(line_ends)

    # Translation to signal space center, Rotation, Backtranslation
    # equals a rotation around the signal space center
    # Here it is assumed that the RGSpace has the same Length as the scanner
    def rotate_Lines(angle, line_starts, line_ends):
        rot_matrix = np.array([[np.cos(angle), -np.sin(angle)],
                               [np.sin(angle), np.cos(angle)]])
        line_starts = rot_matrix.dot(line_starts.T-0.5).T+0.5
        line_ends = rot_matrix.dot(line_ends.T-0.5).T+0.5
        return (line_starts, line_ends)

    def create_rotatedLinesOfSight(number, angle):
        line_starts, line_ends = create_LinesOfSight(number)
        line_starts, line_ends = rotate_Lines(angle, line_starts, line_ends)
        return line_starts, line_ends

    def create_ScanSetup(anglesPerSetup=10, linesPerAngle=10):
        all_angles_LOS = (np.empty((0, 2)), np.empty((0, 2)))
        for angle in [np.pi/anglesPerSetup*j for j in xrange(anglesPerSetup)]:
            one_angle_LOS = create_rotatedLinesOfSight(number=linesPerAngle, angle=angle)
            all_angles_LOS = np.concatenate([all_angles_LOS, one_angle_LOS], axis=1)
        starts = list(all_angles_LOS[0].T)
        ends = list(all_angles_LOS[1].T)
        return (starts, ends)

    # Alternative Setup, randomly probing signal space
    def create_random_los(n=100):
        starts = [np.random.rand(n), np.random.rand(n)]
        ends = [np.random.rand(n), np.random.rand(n)]
        return starts, ends

    def find_Backprojection(R,data):
        empty_field = Field(R.domain, val=0., dtype=d.dtype)
        x_start = empty_field
        CG = ConjugateGradient(iteration_limit=500)
        backprojection, convergence = CG(A=(lambda z: R.adjoint_times(R.times(z))),
                                         b=R.adjoint_times(d),
                                         x0=x_start)
        return backprojection

    distribution_strategy = 'fftw'

    s_space = RGSpace([32, 32], zerocenter=False)
    fft = FFTOperator(s_space)
    h_space = fft.target[0]
    p_space = PowerSpace(h_space, distribution_strategy=distribution_strategy)

    pow_spec = (lambda k: 42/(k+1)**4)

    S = create_power_operator(h_space, power_spectrum=pow_spec,
                              distribution_strategy=distribution_strategy)

    sp = Field(p_space, val=pow_spec,
               distribution_strategy=distribution_strategy)
    sh = sp.power_synthesize(real_signal=True)
    ss = fft.inverse_times(sh)
    #ss = Field(s_space, val=1)

    #x = RGSpace((4,))
    #xss = Field((x, s_space), val=np.arange((16*16*4)).reshape((4,16,16)))
    #ssx = Field((s_space, x), val=np.arange((4*16*16)).reshape((16,16,4)))

if True:
    starts, ends = create_ScanSetup(anglesPerSetup=100, linesPerAngle=150)



    R_fftw = ImagingResponse(s_space, starts=starts, ends=ends,
                         sigmas_up=0.1, sigmas_low=0.1,
                         distribution_strategy='fftw')

    R_not = ImagingResponse(s_space, starts=starts, ends=ends,
                        sigmas_up=0.1, sigmas_low=0.1,
                        distribution_strategy='not')

    R = R_fftw

    noise_factor = 10
    n = Field.from_random(random_type='normal',
                          domain=R.target,
                          std=ss.var()/noise_factor)
    N = DiagonalOperator(domain=R.target,
                         diagonal=np.sqrt(ss.var()/noise_factor))
    d = R(ss) + n

    # Backprojection!
    #backprojection = find_Backprojection(R,d)

    if 1:
        j = R.adjoint_times(N.inverse_times(d))
        D = PropagatorOperator(S=S,N=N, R=R)
        D._InvertibleOperatorMixin__inverter.iteration_limit = 500
        D._InvertibleOperatorMixin__inverter.convergence_tolerance = 0.
        m = D(j)
    if 1:
        m_data = m.val.get_full_data().real
        pl.plot([go.Heatmap(z=m_data)], filename='map_reconstr.html')
        ss_data = ss.val.get_full_data().real
        pl.plot([go.Heatmap(z=ss_data)], filename='map_orig.html')
        #backpr_data = backprojection.val.get_full_data().real
        backpr_data = j.val.get_full_data().real
        pl.plot([go.Heatmap(z=backpr_data)], filename='backprojection.html')

    print "job done"














