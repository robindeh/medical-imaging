import os
from nifty import *
import numpy as np

from nifty.basic_arithmetics import _math_helper


# Data-maker helper
def exp(signals):
    return _math_helper(signals, np.exp)

def drawPoissonDistr(poiss_rate):
    # More efficient but not yet usable:
    # x.val.real.apply_scalar_function(np.random.poisson, inplace=True)
    # result = x
    # return result
    result_val = poiss_rate.val.real.apply_scalar_function(np.random.poisson)
    result = poiss_rate.copy_empty(dtype=result_val.dtype)
    result.val = result_val
    return result


# Minimizer helper to show current energy
def calc_Energy(_lambda_m, _data, _maps, _comp_prior, n_components, measurements, s_space, distribution_strategy):
    #calc likelihood energy
    likelihood_energy = 0
    for e_channel in range(measurements):
        likelihood_energy += np.sum(_lambda_m[e_channel].val.get_full_data().real -
                                   _data[e_channel].val.get_full_data().real * (
                                   np.log(_lambda_m[e_channel].val.get_full_data().real.clip(min=1))))
    #calc prior energy
    prior_energy = 0
    prior_inverse_times = _comp_prior.inverse_times(_maps)
    for i in range(n_components):
        prior_inverse_times_i = Field(domain=s_space, val=prior_inverse_times.val[:, :, i],
                                      distribution_strategy=distribution_strategy, copy=False)
        _maps_i = Field(domain=s_space, val=_maps.val[:, :, i],
                        distribution_strategy=distribution_strategy, copy=False)
        prior_energy += _maps_i.dot(prior_inverse_times_i)

    total_energy = likelihood_energy + prior_energy
    return likelihood_energy, prior_energy, total_energy

def map_ForcesCalc(_S, _R, _maps, _exp_maps, _data, _lambda_m, mu_Es, measurements, s_space, distribution_strategy):
    n_components = _maps.shape[-1]
    result = _maps.copy_empty() * 0

    for e_channel in range(measurements):
        mu_E = mu_Es[e_channel]
        for component in range(n_components):
            _exp_map = Field(domain=s_space, val=_exp_maps.val[:, :, component],
                             distribution_strategy=distribution_strategy, copy=False)
            _mapForce = _R.adjoint_times(_lambda_m[e_channel] - _data[e_channel])
            _mapForce = _mapForce * _exp_map * mu_E[component]
            result.val[:, :, component] += _mapForce.val

    result -= _S.inverse_times(_maps)

    return result

def lambda_m_calc(b_i, R, mu_Es, exp_maps, measurements, s_space, distribution_strategy):
    n_components = exp_maps.shape[-1]
    result_lambdas = [0 for i in range(measurements)]
    for e_channel in range(measurements):
        loss_factor = 0
        mu_E = mu_Es[e_channel]
        for component in range(n_components):
            exp_map = Field(domain=s_space, val=exp_maps.val[:, :, component],
                            distribution_strategy=distribution_strategy, copy=False)
            loss_factor += mu_E[component] * exp_map
        _lambda_m = b_i * exp(-R(loss_factor))
        result_lambdas[e_channel] = _lambda_m
    return result_lambdas