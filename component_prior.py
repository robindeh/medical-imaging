# -*- coding: utf-8 -*-
"""
Created on Thu Apr  6 11:49:48 2017

@author: robin
"""

import inspect
from nifty import *

print inspect.getfile(ProjectionOperator)
import numpy as np
import time

from los_creator import LinesOfSight
print inspect.getfile(create_power_operator)
from nifty_los_response import LOSResponse
import plotly.offline as pl
import plotly.graph_objs as go

from corr_field_drawer import CorrFieldsDrawer


class Component_Prior(EndomorphicOperator):
    
    def __init__(self, power_spectra, operator_space, harmonic_space, s_space, 
                 fftOp, distribution_strategy):

        self.n_components = operator_space.shape[-1]
        self.pow1 = power_spectra['pow1']
        if self.n_components == 2:
            self.pow2 = power_spectra['pow2']
            self.pow12real = power_spectra['pow12real']
            self.pow12imag = power_spectra['pow12imag']
        
        self.s_space = s_space
        self.h_space = harmonic_space
        self.fft = fftOp
        self.unitary = False
        self.symmetric = False
        domain = operator_space
        self.domain = self._parse_domain(domain)
        self.dim = self.infer_dimensionality()
        self.distribution_strategy = distribution_strategy        
        
        self.powOps = self.init_powOp_dict()
        self.inv_powOps = self.init_inv_powOp_dict()
        
        
    def _check_input_compatibility(self, x, spaces, inverse=False):
        """
        The input check must be disabled for the ComposedOperator, since it
        is not easily forecasteable what the output of an operator-call
        will look like.
        """
        return spaces
        
    def _times(self, x, spaces=None):
        
        result = x.copy_empty()*0  
        n_components = x.shape[-1]

        #peforming the matrix multiplication S(phi) where S is a (n_comp x n_comp) 
        #block matrix and phi holds the n_comp signal components.
        for opIndex1 in xrange(n_components):
            for opIndex2 in xrange(n_components):
                signal = Field(domain=self.s_space, val=x.val[:,:,opIndex2],
                               distribution_strategy=self.distribution_strategy, copy=False)
                transformed_sig = self.fft(signal, spaces=spaces)
                y = self.powOps[('S{}{}'.format(opIndex1, opIndex2)
                               )].times(transformed_sig, spaces=spaces)
                result.val[:,:,opIndex1] += self.fft.inverse_times(y, spaces=spaces).val.real

            
        return result

    def _inverse_times(self, x, spaces=None):
        result = x.copy_empty()*0  
        n_components = x.shape[-1]

        #peforming the matrix multiplication S(phi) where S is a (n_comp x n_comp) 
        #block matrix and phi holds the n_comp signal components.
        for opIndex1 in xrange(n_components):
            for opIndex2 in xrange(n_components):
                signal = Field(domain=self.s_space, val=x.val[:,:,opIndex2],
                               distribution_strategy=self.distribution_strategy, copy=False)
                
                transformed_sig = self.fft(signal, spaces=spaces)
                y = self.inv_powOps[('S{}{}'.format(opIndex1, opIndex2)
                               )].times(transformed_sig, spaces=spaces)
                result.val[:,:,opIndex1] += self.fft.inverse_times(y, spaces=spaces).val.real

        return result

    def _k_space_inverse_times(self, x, spaces=None):
        result = x.copy_empty() * 0
        n_components = x.shape[-1]

        # peforming the matrix multiplication S(phi) where S is a (n_comp x n_comp)
        # block matrix and phi holds the n_comp signal components.
        for opIndex1 in xrange(n_components):
            for opIndex2 in xrange(n_components):
                signal = Field(domain=self.h_space, val=x.val[:, :, opIndex2],
                               distribution_strategy=self.distribution_strategy, copy=False)
                y = self.inv_powOps[('S{}{}'.format(opIndex1, opIndex2)
                                     )].times(signal, spaces=spaces)

                result.val[:, :, opIndex1] = result.val[:, :, opIndex1] + y.val

        return result

    def init_powOp_dict(self):
        powOp_dict = {}
        powOp_dict['S00'] = create_power_operator(self.h_space, power_spectrum=self.pow1,
                              distribution_strategy=self.distribution_strategy)
                             
        if self.n_components == 2:
            powOp_dict['S11'] = create_power_operator(self.h_space, power_spectrum=self.pow2,
                                  distribution_strategy=self.distribution_strategy)
            powOp_dict['S01'] = create_power_operator(self.h_space, power_spectrum=self.pow12real,
                                  distribution_strategy=self.distribution_strategy)
            powOp_dict['S10'] = powOp_dict['S01']
        return powOp_dict
        
    def init_inv_powOp_dict(self):
        inv_powOp_dict = {}
        
        #Careful: correlation should never be exactly 1 for any k. This will lead
        #to the S11 term becoming infinite!                
                
        if self.n_components == 1:
            pow_inv_11 = (lambda k: 1./( self.pow1(k)))   
            inv_powOp_dict['S00'] = create_power_operator(self.h_space, 
                            power_spectrum=pow_inv_11, 
                            distribution_strategy=self.distribution_strategy) 
        
        if self.n_components == 2:
            
            pow_inv_11 = (lambda k: 1./( self.pow1(k)-self.pow12real(k)* self.pow12real(k) / self.pow2(k)))
            pow_inv_22 = (lambda k: 1./(self.pow2(k)-self.pow12real(k)* self.pow12real(k) / self.pow1(k)))
            pow_inv_12 = (lambda k: (-self.pow12real(k)/self.pow1(k))*(1./(self.pow2(k)- 
                                    self.pow12real(k)*self.pow12real(k)/self.pow1(k))))
            pow_inv_21 = (lambda k: (-self.pow12real(k)/self.pow2(k))*(1./(self.pow1(k) -
                                   self.pow12real(k)*self.pow12real(k)/self.pow2(k))))
            
            
            inv_powOp_dict['S00'] = create_power_operator(self.h_space, 
                    power_spectrum=pow_inv_11, 
                    distribution_strategy=self.distribution_strategy)
            inv_powOp_dict['S11'] = create_power_operator(self.h_space, 
                    power_spectrum=pow_inv_22,
                                  distribution_strategy=self.distribution_strategy)
            inv_powOp_dict['S01'] = create_power_operator(self.h_space, power_spectrum=pow_inv_12,
                                  distribution_strategy=self.distribution_strategy)
            inv_powOp_dict['S10'] = create_power_operator(self.h_space, power_spectrum=pow_inv_21,
                                  distribution_strategy=self.distribution_strategy)
        
        return inv_powOp_dict
        
    def infer_dimensionality(self):
        dim = len(self.h_space.shape)
        return dim
        
    def symmetric(self):
        return False
    def unitary(self):
        return False
    def domain(self):
        return self.domain
    def implemented(self):
        return True
    def self_adjoint(self):
        raise exception('This is not implemented for this class and should not be necessary')



if __name__ == "__main__":
    np.random.seed(22)
    # Create mock signal and covariance
    
    losCreator = LinesOfSight()
    anglesPerSetup, linesPerAngle = 20,32
    data_vec_length = anglesPerSetup*linesPerAngle
    starts, ends = losCreator.create_RotatedScanSetup(anglesPerSetup, linesPerAngle)
    
    
    distribution_strategy = "not"
    n_components = 2
    dimensions = [128, 128]
    s_space = RGSpace(dimensions)
    operator_space = RGSpace(dimensions+[n_components])
    fft = FFTOperator(s_space, target_dtype=np.complex128)
    h_space = fft.target[0]
    p_space = PowerSpace(h_space, distribution_strategy=distribution_strategy)
    
    #Create correlation structure: positive correlation for large scales,
    #negative correlation for small scales. "large" and "small" are 
    #determined by the corr_length_k
    k_vec_length = p_space.shape[0]    
    corr_length_k = int(k_vec_length*0.2) 
     
    def corr(k,corr_length_k):
        a = np.zeros_like(k)
        a = a + 0.99
        a[corr_length_k:] = -a[corr_length_k:]
        return a
    
    def cross_pow_spectrum(k, pow1, pow2 ):
        result = np.sqrt(pow1(k)*pow2(k))
        result = result*corr(k, corr_length_k)
        return result
       
    #  Make power spectra dictionary
    pow_spectra = {}
    pow_spectra['pow1'] = (lambda k: 1/((k+1)**2))
    pow_spectra['pow2'] = (lambda k: 3/((k+1)**3))
    pow_spectra['pow12real'] = (lambda k: cross_pow_spectrum(k, pow_spectra['pow1'],
                                 pow_spectra['pow2'] ))
    pow_spectra['pow12imag'] = (lambda k: k*0.)
    
    
    fieldDrawer = CorrFieldsDrawer(pow_space=p_space, distr_strat=distribution_strategy,
                                   n_components=n_components)
    sh1, sh2 = fieldDrawer.draw_fields(pow_spectra)
    ss1, ss2 = fft.inverse_times(sh1), fft.inverse_times(sh2)
    
    signals_vals = np.empty(dimensions+[n_components])
    signals_vals[...,0] = ss1.val
    signals_vals[...,1] = ss2.val
    
    signals = Field(operator_space, val=signals_vals, 
                    distribution_strategy=distribution_strategy)
    print signals.shape

    prior = Component_Prior(pow_spectra, operator_space, h_space, s_space, fft, distribution_strategy)
    #print prior(signals)
    
    print signals - prior(prior.inverse_times(signals)) #check SS^(-1) = 1
    ss1, ss2 = fft.inverse_times(fft(ss1)), fft.inverse_times(fft(ss2))
    transf_signals_vals = np.empty(dimensions+[n_components])
    transf_signals_vals[...,0] = ss1.val
    transf_signals_vals[...,1] = ss2.val
    transf_signals = Field(operator_space, val=transf_signals_vals, 
                    distribution_strategy=distribution_strategy)
    print signals - transf_signals #Check if fft performs correctly  


    summe = 0
    amount = 1000
    for i in xrange(amount):
        sh1, sh2 = fieldDrawer.draw_fields(pow_spectra, rand_seed=np.random.randint(0,10000))
        signals_vals = np.empty(dimensions + [n_components])
        signals_vals[..., 0] = sh1.val
        signals_vals[..., 1] = sh2.val
        signals = Field(operator_space, val=signals_vals,
                        distribution_strategy=distribution_strategy)
        signals_conj = signals.conjugate(inplace=False)

        summe = summe + signals_conj * (prior._k_space_inverse_times(signals))
    sum_field = summe / amount
    for comp in xrange(n_components):
        a = Field(h_space, val=sum_field.val[:,:,comp])
        print a
        print 'The mean of the above field should roughly be 0.5 for each field'

    print 'job done'
