# -*- coding: utf-8 -*-
"""
Created on Mon Apr 24 16:28:37 2017

@author: robin
"""

from nifty import *

import numpy as np

from los_creator import LinesOfSight
from nifty_los_response import LOSResponse
from nifty.basic_arithmetics import _math_helper
import plotly.offline as pl
import plotly.graph_objs as go

from corr_field_drawer import CorrFieldsDrawer


class Component_Posterior(InvertibleOperatorMixin, EndomorphicOperator):
    
    def __init__(self, comp_prior, R, exp_maps, lambda_m, mu_Es, b_i, data, measurements, s_space,
                 distribution_strategy, inverter=None, preconditioner=None):
        
        #nifty stuff
        self.s_space = s_space
        self.unitary = False
        self.symmetric = False
        domain = comp_prior.domain
        self.domain = self._parse_domain(domain)
        self.distribution_strategy = distribution_strategy

        
        #save all variables and operators
        self._S = comp_prior
        self.R = R
        self.mu_Es = mu_Es
        self.exp_m = exp_maps
        self.data = data
        self.b_i = b_i
        self.lambda_m = lambda_m
        self.measurements = measurements
     
        
        #conjugate gradient/ minizer stuff
        if preconditioner is None:
            preconditioner = self._S.times
        super(Component_Posterior, self).__init__(inverter=inverter,
                                                 preconditioner=preconditioner)
        
    def _check_input_compatibility(self, x, spaces, inverse=False):
        """
        The input check must be disabled for the ComposedOperator, since it
        is not easily forecasteable what the output of an operator-call
        will look like.
        """
        return spaces
        
    def _inverse_times(self, x, spaces=None):
        
        result = x.copy_empty()*0  
        n_components = x.shape[-1]
        
        # peforming the matrix multiplication M(x) where M is a (n_comp x n_comp)
        # block matrix and x holds the n_comp signal components.
        for e_channel in range(self.measurements):
            mu_E = self.mu_Es[e_channel]
            for opIndex1 in range(n_components):
                for opIndex2 in range(n_components):

                    #Bring fields into suitable form
                    input_Field = Field(domain=self.s_space, val=x.val[:,:,opIndex2],
                                   distribution_strategy=self.distribution_strategy, copy=False)
                    interim_result = input_Field.copy_empty()*0

                    exp_m_left = Field(domain=self.s_space, val=self.exp_m.val[:,:,opIndex1],
                                   distribution_strategy=self.distribution_strategy, copy=False)
                    exp_m_right = Field(domain=self.s_space, val=self.exp_m.val[:,:,opIndex2],
                                   distribution_strategy=self.distribution_strategy, copy=False)

                    #The actual calculation
                    interim_result = interim_result + exp_m_left*mu_E[opIndex1]* self.R.adjoint_times(
                             (self.lambda_m[e_channel])*self.R(mu_E[opIndex2]*exp_m_right*input_Field))

                    # This part causes the optimization scheme to become unstable
                    # dataLambda_positive = (self.data - self.lambda_m)
                    # dataLambda_positive.val = dataLambda_positive.val.get_full_data().clip(min=0)

                    # lambdaData_positive =  self.data - self.lambda_m
                    # lambdaData_positive.val = lambdaData_positive.val.get_full_data().clip(min=0)

                    #if opIndex1 == opIndex2:
                    #    interim_result = interim_result + (
                    #            self.R.adjoint_times(dataLambda_positive))*exp_m_right*self.mu_Es[opIndex2]*input_Field

                    result.val[:,:,opIndex1] = result.val[:,:,opIndex1] + interim_result.val

        result = result + self._S.inverse_times(x)
        return result
        
    def infer_dimensionality(self):
        dim = len(self.h_space.shape)
        return dim
        
    def symmetric(self):
        return False
    def unitary(self):
        return False
    def domain(self):
        return self.domain
    def implemented(self):
        return True
    def self_adjoint(self):
        raise exception('Not implemented for this class and should not be necessary')
        
        
if __name__ == "__main__":
    
    from component_prior import Component_Prior    
    from nifty_los_response import LOSResponse
    from los_creator import LinesOfSight
    from imaging_response import ImagingResponse
    
    np.random.seed(42)
    # Create mock signal and covariance
    
    losCreator = LinesOfSight()
    anglesPerSetup, linesPerAngle = 20,32
    data_vec_length = anglesPerSetup*linesPerAngle
    starts, ends = losCreator.create_RotatedScanSetup(anglesPerSetup, linesPerAngle)
    
    
    distribution_strategy = "not"
    n_components = 1
    dimensions = [65, 65]
    s_space = RGSpace(dimensions)
    operator_space = RGSpace(dimensions+[n_components])
    fft = FFTOperator(s_space, target_dtype=np.complex128)
    h_space = fft.target[0]
    p_space = PowerSpace(h_space, distribution_strategy=distribution_strategy)
    
    #Create correlation structure: positive correlation for large scales,
    #negative correlation for small scales. "large" and "small" are 
    #determined by the corr_length_k
    k_vec_length = p_space.shape[0]    
    corr_length_k = int(k_vec_length*0.2) 
     
    def corr(k,corr_length_k):
        a = np.zeros_like(k)
        a = a + 0.99
        a[corr_length_k:] = -a[corr_length_k:]
        return a
    
    def cross_pow_spectrum(k, pow1, pow2 ):
        result = np.sqrt(pow1(k)*pow2(k))
        result = result*corr(k, corr_length_k)
        return result
       
    #  Make power spectra dictionary
    pow_spectra = {}
    pow_spectra['pow1'] = (lambda k: 10/((k+1)**2))
    pow_spectra['pow2'] = (lambda k: 100/((k+1)**3))
    pow_spectra['pow12real'] = (lambda k: cross_pow_spectrum(k, pow_spectra['pow1'],
                                 pow_spectra['pow2'] ))
    pow_spectra['pow12imag'] = (lambda k: k*0.)
    
    
    fieldDrawer = CorrFieldsDrawer(pow_space=p_space, distr_strat=distribution_strategy,
                                   n_components=n_components)
    sh1, sh2 = fieldDrawer.draw_fields(pow_spectra, rand_seed=250)
    signal_list = [fft.inverse_times(sh1), fft.inverse_times(sh2)]

    signals_vals = np.empty(dimensions+[n_components])
    for component in xrange(n_components): 
        signals_vals[...,component] = signal_list[component].val
    
    signals = Field(operator_space, val=signals_vals, 
                    distribution_strategy=distribution_strategy)
    
    
    
    losCreator = LinesOfSight()
    anglesPerSetup, linesPerAngle = 20,32
    data_vec_length = anglesPerSetup*linesPerAngle
    starts, ends = losCreator.create_RotatedScanSetup(anglesPerSetup, linesPerAngle)
        
    # Create mock data from given signal and covariance
    R = ImagingResponse(s_space, starts=starts, ends=ends,
                            sigmas_up=0.1, sigmas_low=0.1,
                            distribution_strategy=distribution_strategy)
    S = Component_Prior(pow_spectra, operator_space, h_space, s_space, fft, distribution_strategy)
    mu_Es = [1,10]
    b_i = 1000
    def exp(signals, inplace=False):         
        return _math_helper(signals, np.exp)
     
                   
    def drawPoissonDistr(poiss_rate):
        # More efficient but not yet usable:
        #x.val.real.apply_scalar_function(np.random.poisson, inplace=True)
        #result = x
        #return result
        result_val = poiss_rate.val.real.apply_scalar_function(np.random.poisson)
        result = poiss_rate.copy_empty(dtype=result_val.dtype)
        result.val = result_val
        return result
    
    exp_signals = exp(signals)
    mu_Es = [1,10]
    
    b_i = 1000
    background = 1
    background = [np.random.poisson(background) for i in xrange(data_vec_length)]
    
    absoprtion_factor = 0
    for compIndex in xrange(n_components):
        exp_signal = Field(domain=s_space, val=exp_signals.val[:,:,compIndex],
                       distribution_strategy=distribution_strategy, copy=False)
        absoprtion_factor += mu_Es[compIndex]*exp_signal 
      
    poisson_rate = b_i * exp(-R(absoprtion_factor)) + background
    data = drawPoissonDistr(poisson_rate)

    def lambda_m_calc(b_i, R, mu_Es, exp_maps, data):
        
        n_components = exp_maps.shape[-1]
        loss_factor = 0
        
        for component in range(n_components):
            exp_map = Field(domain=s_space, val=exp_maps.val[:,:,component],
                               distribution_strategy=distribution_strategy, copy=False)
            loss_factor += mu_Es[component]*exp_map
            
        _lambda_m = b_i * exp(-R(loss_factor))
        return _lambda_m
        
    lambda_m = lambda_m_calc(b_i, R, mu_Es, exp_signals, data)
    #print 'signals', signals

    D = Component_Posterior(S, R, exp_signals, lambda_m , mu_Es, b_i, data, s_space, distribution_strategy)
    
    print signals - D(D.inverse_times(signals))
    print signals - D.inverse_times(D(signals))
    print 'job done'
    
    
    
    
    
    
    
    
    
    
    
    
    