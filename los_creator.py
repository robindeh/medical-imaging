# -*- coding: utf-8 -*-
"""
Created on Fri Mar  3 11:38:23 2017

@author: robin
"""
import numpy as np

class LinesOfSight:
    def __init__(self):
        None
        
    def _create_LinesOfSight(self, linesPerAngle):
        line_starts = []
        line_ends = []
        for i in xrange(linesPerAngle):
            # lines should fall inbetween the i's, therefore add 0.5
            i = float(i+0.5)
            line_starts += [[(-0.25), (-0.25 + 1.5*i/linesPerAngle)]]
            line_ends +=   [[(1.25) , (-0.25 + 1.5*i/linesPerAngle)]]
        return np.array(line_starts), np.array(line_ends)

    # Translation to signal space center, Rotation, Backtranslation 
    # equals a rotation around the signal space center
    # Here it is assumed that the RGSpace has Lenght 1, therefore 0.5 is added/subtracted
    def _rotate_Lines(self, angle, line_starts, line_ends):
        rot_matrix = np.array([[np.cos(angle), -np.sin(angle)],
                               [np.sin(angle), np.cos(angle)]])
        line_starts = rot_matrix.dot(line_starts.T-0.5).T +0.5
        line_ends = rot_matrix.dot(line_ends.T-0.5).T +0.5

        return (line_starts, line_ends)
    
    def _create_rotatedLinesOfSight(self, linesPerAngle, angle):
        line_starts, line_ends = self._create_LinesOfSight(linesPerAngle)
        line_starts, line_ends = self._rotate_Lines(angle, line_starts, line_ends)
        return line_starts, line_ends

    def create_RotatedScanSetup(self, anglesPerSetup=10, linesPerAngle=10):       
        all_angles_LOS = (np.empty((0, 2)), np.empty((0, 2)))
        for angle in [np.pi/anglesPerSetup*j for j in xrange(anglesPerSetup)]:
            one_angle_LOS = self._create_rotatedLinesOfSight(linesPerAngle=linesPerAngle, angle=angle)
            all_angles_LOS = np.concatenate([all_angles_LOS, one_angle_LOS], axis=1)
        starts = list(all_angles_LOS[0].T)
        ends = list(all_angles_LOS[1].T)
        return (starts, ends)


if __name__ == '__main__':
    import plotly.offline as pl
    import plotly.graph_objs as go

    losCreator = LinesOfSight()
    (anglesPerSetup, linesPerAngle) = (4, 10)
    starts, ends = losCreator.create_RotatedScanSetup(anglesPerSetup, linesPerAngle)
    print np.array(starts)[:,0]
    print np.array(starts)[0, 0:20]


    for i in xrange(anglesPerSetup):

        point_interval_start = i*linesPerAngle
        point_interval_end = (i+1)*linesPerAngle
        print point_interval_end

        trace0 = go.Scatter(
            x=np.array(starts)[0,point_interval_start:point_interval_end],
            y=np.array(starts)[1,point_interval_start:point_interval_end],
            mode='markers',
            name='{} start points'.format(linesPerAngle)
        )
        trace1 = go.Scatter(
            x=np.array(ends)[0,point_interval_start:point_interval_end],
            y=np.array(ends)[1,point_interval_start:point_interval_end],
            mode='markers',
            name='{} end points'.format(linesPerAngle))

        traces = [trace0, trace1]
        pl.plot(traces, filename='LOS_creator_lines{}.html'.format(i*linesPerAngle), auto_open=False)