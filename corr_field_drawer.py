# -*- coding: utf-8 -*-
"""
Created on Fri Mar 31 11:34:47 2017

@author: robin
"""

from __future__ import division
from nifty import *
#import plotly.offline as pl
#import plotly.graph_objs as go

import numpy as np

from keepers import Versionable,\
                    Loggable

from d2o import distributed_data_object,\
    STRATEGIES as DISTRIBUTION_STRATEGIES

from nifty.config import nifty_configuration as gc

from nifty.domain_object import DomainObject

from nifty.spaces.power_space import PowerSpace

import nifty.nifty_utilities as utilities
from nifty.random import Random

import plotly.offline as pl
import plotly.graph_objs as go

from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.rank
np.random.seed(42)



class CorrFieldsDrawer():
    
    def __init__(self, pow_space, distr_strat, n_components=2):
        self.n_components = n_components
        self.pow_space = pow_space
        self.k_vec_length = pow_space.shape[0]
        self.distr_strat = distr_strat

    def draw_fields(self, pow_spectra, rand_seed=42):
        pow_spec1 = pow_spectra['pow1']
        pow_spec2 = pow_spectra['pow2']
        pow_spec12real = pow_spectra['pow12real']
        pow_spec12imag = pow_spectra['pow12imag']
        
        sp1_spec =        (lambda k: np.sqrt(pow_spec1(k)))
        # Create spectra according to lower/upper matrix decomposition
        cross_spec_real = (lambda k: pow_spec12real(k)/sp1_spec(k))
        cross_spec_imag = (lambda k: pow_spec12imag(k)/sp1_spec(k))
        sp2_spec =        (lambda k: np.sqrt((pow_spec2(k) - cross_spec_real(k)**2
                                        - cross_spec_imag(k)**2).clip(min=0)))
                                          
        sp1 = Field(domain=self.pow_space,val=sp1_spec,
                distribution_strategy=self.distr_strat)
        sp1_real = Field(domain=self.pow_space, val=cross_spec_real,
               distribution_strategy=self.distr_strat)
        sp1_im = Field(domain=self.pow_space, val=cross_spec_imag,
               distribution_strategy=self.distr_strat)
        sp2 = Field(domain=self.pow_space, val=sp2_spec,
               distribution_strategy=self.distr_strat)
        
        # Repeat random seed in order to draw identical random field for all s1's
        np.random.seed(rand_seed)
        sig_harm1 = sp1.power_synthesize(real_signal=True)
        np.random.seed(rand_seed)
        sig_harm1_real = sp1_real.power_synthesize(real_signal=True)
        np.random.seed(rand_seed)
        sig_harm1_im = sp1_im.power_synthesize(real_signal=True)
        sig_harm2 = sp2.power_synthesize(real_signal=True)

        #sig_harm1 is now already the correct field 1. The wanted correlated field2
        #is now constructed from the above drawn fields sig_harm1_real/im and sig_harm2
        result_field1 = sig_harm1
    
        result_field2_val = 1j*(sig_harm2.val.imag + sig_harm1_real.val.imag - sig_harm1_im.val.real)
        result_field2_val += sig_harm2.val.real + sig_harm1_im.val.imag + sig_harm1_real.val.real
        sig_harm2.val = result_field2_val
        result_field2 = sig_harm2
    
        return result_field1, result_field2
        

    
if __name__ == "__main__":
                            
    distribution_strategy = 'not'
    n_components = 2
    rand_seed = 106
    np.random.seed(rand_seed)
    
    # Setting up the geometry
    s_space = RGSpace([256, 256])
    fft = FFTOperator(s_space)
    h_space = fft.target[0]
    p_space = PowerSpace(h_space, distribution_strategy=distribution_strategy)
    
    k_vec_length = p_space.shape[0]
    corr_length_k_short = int(k_vec_length * 0.0002)  # 0.0002 = short, 0.004=long
    corr_length_k_long = int(k_vec_length * 0.004)
    corr_length_k = corr_length_k_short #0.0002 = short

    #print corr_length_k
    #corr_length_k = 0    


    def corr(k, corr_length_k):
        sig_height = 0.8
        sig_translation = corr_length_k
        sig_k = 2 * ((1. / (1 + np.exp(k - k[sig_translation]))) - 0.5)
        sig_k = sig_k * sig_height
        return sig_k

    """
    def corr(k, corr_length_k):
        a = np.zeros_like(k)
        a = a + 0.80
        a[corr_length_k:] = -a[corr_length_k:]
        return a
    """

    def cross_pow_spectrum(k, pow1, pow2 ):
        result = np.sqrt(pow1(k)*pow2(k))
        result = result*corr(k, corr_length_k)
        return result
    
    pow_spectra = {}
    #pow_spectra['pow1'] = (lambda k: 10/((k+1)**2))
    #pow_spectra['pow2'] = (lambda k: 20/((k+1)**3))
    pow_spectra['pow1'] = (lambda k: .0009 / ((k / 4. + .5) ** 5))
    pow_spectra['pow2'] = (lambda k: .0006 / ((k / 4. + .5) ** 5))
    pow_spectra['pow12real'] = (lambda k: cross_pow_spectrum(k, pow_spectra['pow1'],
                                 pow_spectra['pow2'] ))
    pow_spectra['pow12imag'] = (lambda k: k*0.)    
        
    fieldDrawer = CorrFieldsDrawer(pow_space=p_space, distr_strat=distribution_strategy,
                                   n_components=n_components)
                                   
    sh1, sh2 = fieldDrawer.draw_fields(pow_spectra, rand_seed=rand_seed)
                            
    ss1 = fft.inverse_times(sh1)
    ss2 = fft.inverse_times(sh2)

    trace0 = go.Scatter(
        x = p_space.kindex,
        y = sh1.power_analyze().val.get_full_data().real,
        mode = 'lines',
        name = 'field_pow_spec s1'
    )
    trace1 = go.Scatter(
        x = p_space.kindex,
        y = pow_spectra['pow1'](p_space.kindex),
        mode = 'lines',
        name = 'expected pow_spec s1'
    )
    
    data = [trace0,trace1]
    pl.plot(data, filename='corr_field_draw_test1.html', auto_open=False)
    
    cross_spec_real = (lambda k: pow_spectra['pow12real'](k)**2/pow_spectra['pow1'](k))
    cross_spec_imag = (lambda k: pow_spectra['pow12imag'](k)**2/pow_spectra['pow1'](k))
    sp2_spec =        (lambda k: (pow_spectra['pow2'](k) - cross_spec_real(k) 
                                      - cross_spec_imag(k)).clip(min=0))     
    
    kindex_start = 0
    kindex_array = p_space.kindex[kindex_start:]    
    
    trace0 = go.Scatter(
        x = kindex_array,
        y = sh2.power_analyze().val.get_full_data().real[kindex_start:],
        mode = 'lines',
        name = 'field_pow_spec s2'
    )
    trace1 = go.Scatter(
        x = kindex_array,
        y = pow_spectra['pow2'](kindex_array),
        mode = 'lines',
        name = 'S2'
    )
    trace2 = go.Scatter(
        x = kindex_array,
        y = sp2_spec(kindex_array),
        mode = 'lines',
        name = 'expected for this field ideally'
    )
    trace3 = go.Scatter(
        x = kindex_array,
        y = corr(kindex_array, corr_length_k),
        mode = 'lines',
        name = 'correlation'
    )
    data = [trace3] # trace0,trace1,trace2,
    pl.plot(data, filename='corr_field_draw_test2.html', auto_open=False)

   
    ss1_data = np.exp(ss1.val.get_full_data().real)
    pl.plot([go.Heatmap(z=ss1_data)], filename='corr_field_draw_test3.html', auto_open=False)
    
    ss2_data = np.exp(ss2.val.get_full_data().real)
    pl.plot([go.Heatmap(z=ss2_data)], filename='corr_field_draw_test4.html',auto_open=False)  
    
    print 'job done.'